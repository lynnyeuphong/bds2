<?php
require("home/header/header.php");
?>
<script>
  function checkForm()
  {
   var diadiemgiaohang = document.forms['insertInvoice']["diadiemgiaohang"].value;
   var ngay = document.forms['insertInvoice']["ngay"].value;
   var gio = document.forms['insertInvoice']["gio"].value;
   var phut = document.forms['insertInvoice']["phut"].value;
   var thanhtoan = document.forms['insertInvoice']["thanhtoan"].value;

   if(diadiemgiaohang == '')
   {
    alert('Bạn phải nhập địa điểm');
    document.forms["insertInvoice"]["diadiemgiaohang"].focus();
    return false;
  }
  else if(ngay == '')
  {
    alert('Bạn phải nhập ngày');
    document.forms["insertInvoice"]["ngay"].focus();
    return false;
  }
  else if(gio == '')
  {
    alert('Bạn phải nhập giờ');
    document.forms["insertInvoice"]["gio"].focus();
    return false;
  }
  else if(phut == '')
  {
    alert('Bạn phải nhập phút');
    document.forms["insertInvoice"]["phut"].focus();
    return false;
  }
  else if(thanhtoan == '')
  {
    alert('Bạn phải nhập hình thức thanh toán !');
    document.forms["insertInvoice"]["thanhtoan"].focus();
    return false;
  }
  else return true;
}
</script>
<link rel="stylesheet" type="text/css" href="bootstrap/dist/css/bootstrap.min.css">
<!-- Page title -->
<div class="page-title"  style="padding-top: 130px;">
 <div class="container">
  <h3><i class="icon-shopping-cart color"></i> Giỏ hàng <small>Mua hàng !!!</small></h3>
  <hr />
</div>
</div>
<!-- Page title -->

<div class="view-cart blocky">
 <div class="container">
  <div class="row">
   <div class="col-md-12">

    <!-- Table -->
    <?php
    if($_SESSION['cart']['buying-soluong'] > 0) { ?>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>STT</th>
            <th>Thông tin sản phẩm</th>
            <th>Ảnh</th>
            <th>Số lượng</th>
            <th>Đơn giá</th>
            <th>Thành tiền</th>
          </tr>
        </thead>
        <tbody>
          <?php
                            //if($_SESSION['cart']['buying-soluong'] > 0) {
          $carts = $_SESSION['cart']['products'];
          $i = 1;
          $k = 0;
          foreach($carts as $cart)
          {  
           $j = $cart['add-soluong']*$cart['gia'];
           $k = $k + $j;
         }
         ?>

         <tr class="<?php echo ($i%2 == 0)?"odd":"even";   ?>">

          <!-- Index -->
          <td><?=$i++ ?></td>
          <!-- Product  name -->
          <td><a href="#"><?=$cart['tensp'] ?></a></td>
          <!-- Product image -->
          <td><a href="#"><img src="<?=$cart['anh'] ?>" alt="ảnh sp" class="img-responsive" style="height:30px; width:50px"/></a></td>
          <!-- Quantity with refresh and remove button -->
          <td><?=$cart['add-soluong'] ?></td>
          <!-- Unit price -->
          <td><?=$cart['gia'] ?></td>
          <!-- Total cost -->
          <td><?=$cart['add-soluong']*$cart['gia']?></td> 
          <td>
            <a href="?page=your-cart&action=xoasanpham&idsp=<?=$cart['id'] ?>"><button class="btn btn-danger" type="button"><i class="icon-remove"></i></button></a>
          </td>                       
        </tr>
      <?php }  ?>


      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><strong>Tổng tiền</strong></td>
        <td> <strong> <?= $k ?></strong></td>
      </tr>
    </tbody>
  </table>
  <!-- Địa điểm, thời gian giao hàng-->

  <div class="checkout">

   <div class="container">
    <h4>Giao hàng</h4>
    <br />
    <form class="form-horizontal" role="form" name="insertInvoice" method="post" action="?page=your-cart&action=insertInvoice" onsubmit="return checkForm()">

      <div class="form-group">
        <label for="inputZip" class="col-md-2 control-label">Địa điểm giao * </label>
        <div class="col-md-4">
          <input type="text" class="form-control" name="diadiemgiaohang">
        </div>
      </div>


      <div class="form-group">
        <label for="inputPhone" class="col-md-2 control-label">Ngày giờ nhận * </label>
        <div class="col-md-4">
          <table style="width: 550px;">
            <tr>
              <?php 
              $now = getdate();
              $currentDate = $now["year"] . "-" . $now["mon"] . "-" . $now["mday"]; 
              ?>
              <td><label for="inputPhone" class="col-md-2 control-label">Ngày: </label></td>
              <td><input style="width:90px;" type="text" class="form-control" name="ngay" value="<?=$currentDate?>"></td>

              <td><label for="inputPhone" class="col-md-2 control-label">Giờ: </label></td>
              <td>
                <select style="width:60px;" class="form-control" name="gio">
                  <option value="" >--giờ-</option>
                  <?php 
                  for ($hour = 00; $hour<24; $hour++)
                    { ?>
                      <option value="<?=$hour?>"><?=$hour?></option>
                    <?php  }
                    ?>
                  </select>
                </td>

                <td><label for="inputPhone" class="col-md-2 control-label">Phút: </label></td>
                <td>
                  <select style="width:60px;" class="form-control" name="phut">
                    <option value="" >--Phút--</option>
                    <?php 
                    for ($minutes = 00; $minutes<60; $minutes++)
                      { ?>
                        <option value="<?=$minutes?>"><?=$minutes?></option>
                      <?php  }
                      ?>
                    </select>
                  </td>
                </tr>
              </table>
            </div>
          </div>

          <div class="form-group">
            <label for="inputCountry" class="col-md-2 control-label">Thanh toán:</label>
            <div class="col-md-4">
              <select class="form-control" name="thanhtoan">
                <option value="" >--thanh toán--</option>
                <option>Tiền mặt</option>
                <option>Chuyển khoản</option>
              </select>
            </div>
          </div> 

          <div class="form-group">
            <label for="inputAddress" class="col-md-2 control-label">Ghi chú thêm:</label>
            <div class="col-md-4">
             <textarea class="form-control" rows="3" name="ghichu"></textarea>
           </div>
         </div>

         <hr />             

         <div class="form-group">
          <div class="col-md-offset-2 col-md-10">
            <button class="btn btn-danger" >Đặt hàng</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <?php 

  ?>




  <div class="sep-bor"><center><img src="imageIcon/giohang.jpg" alt=""></center></div>
</div>
</div>
</div>
</div>

<!-- Recent posts CarouFredSel Starts -->


<?php
require("home/footer/footer.php");
?>