<?php 
include('config/config.php');
$ketnoi = mysqli_connect("localhost", "root", "", "bds_hathanh");
$sql="SELECT * FROM tbl_gioithieu";
?>
<head>
	<link rel="stylesheet" href="home/footer/footer.css">
	<link rel="stylesheet" href="js/font-awesome/css/font-awesome.min.css">
	<?php include_once ('home/contact/contact.php'); ?>	
	<footer class="footer">
		<div class="item-footer logo_footer">
			<div class="ft-logo">
				<img src="images/logo.png">
			</div>
			<div class="linetext linetext-bottom">
				<a href="https://www.facebook.com/theminatoresidence/"><i class="fa fa-facebook"></i></a> 
				<a href="https://www.youtube.com/channel/uco1hqqwtqxfaey1bwo5v_mq"><i class="fa fa-youtube-play"></i> </a>
			</div>
		</div>
		<div class="item-footer">
			<?php 
			$sql="SELECT * FROM tbl_diachiduan"; 
			$query = $ketnoi->query($sql);
			$rows = $query->fetch_assoc()
				?>
				<div class="title-footer">địa chỉ dự án</div>
				<div class="linetext">
					<i class="fa fa-map-marker"></i>
					<p><?php echo $rows['diachi'] ?></p>
				</div>
				<a href="tel:1800577783" class="linetext">
					<i class="fa fa-phone"></i>
					<p class="hot_line"><?php echo $rows['sodienthoai'] ?></p>
				</a>
				<a href="mailto:minato.info@waterfrontcity.vn" class="linetext">
					<i class="fa fa-envelope"></i>
					<p><?php echo $rows['email'] ?></p>
				</a>
			</div>
			<div class="item-footer">
				<?php 
			$sql="SELECT * FROM tbl_diachigiaodich"; 
			$query = $ketnoi->query($sql);
			$rows = $query->fetch_assoc()
				?>
				<div class="title-footer">địa chỉ giao dịch</div> 
				<div class="linetext">
					<i class="fa fa-map-marker"></i>
					<p><?php echo $rows['diachi'] ?></p>
				</div>
				<a href="tel:0936777288" class="linetext">
					<i class="fa fa-phone"></i>
					<p><?php echo $rows['sodienthoai'] ?></p>
				</a>
				<a href="mailto:maianh280@gmail.com" class="linetext">
					<i class="fa fa-envelope"></i>
					<p><?php echo $rows['email'] ?></p>
				</a>
			</div>
			<div class="item-footer">
				<i class="fa fa-copyright"></i>copyright © 2019 BDS HÀ THÀNH. DESIGN BY nhóm 29.
			</div>

		</footer>
		<script>
			$(document).ready(function(){
				var scrollTop = 0;
				$(window).scroll(function(){
					scrollTop = $(window).scrollTop();
					$('.counter').html(scrollTop);

					if (scrollTop >= 130) {
						$('.header').addClass('scrolled-header');
						$('.logo').addClass('scrolled-logo');
						$('.nav-header').addClass('scrolled-nav-header');
						$('.header-top').addClass('scrolled-header-top');
					} else if (scrollTop < 130) {
						$('.header').removeClass('scrolled-header');
						$('.logo').removeClass('scrolled-logo');
						$('.nav-header').removeClass('scrolled-nav-header');
						$('.header-top').removeClass('scrolled-header-top');
					} 					
				}); 
			});
		</script>
	</body>
	</html>
