<?php 
include('config/config.php');
$ketnoi = mysqli_connect("localhost", "root", "", "bds_hathanh");
$sql="SELECT * FROM tbl_gioithieu";
?>
<link rel="stylesheet" type="text/css" href="styles.css">
<div class="intro">
	<?php  
	$query = $ketnoi->query($sql);
	while ($rows = $query->fetch_assoc()){
		?>
		<div class="title">
			<h3>Giới thiệu về <?php echo $rows['ten'] ?></h3>
			<p><?php echo $rows['slogan'] ?></p>
		</div>
		<div class="content">
			<div class="content-left">
				<div class="swiper-container " id="swiper-intro">
					<div class="swiper-wrapper">

						<div class="swiper-slide"><a href=""><img src="admin/quanly/uploads/<?php echo $rows['anh'] ?>" alt=""></a></div>

						
					</div>
					<div class="swiper-pagination" id="pagination-intro">
					</div>
				</div>
			</div>
			<div class="content-right">
				<?php echo  html_entity_decode($rows['noidung']) ?>

			</div>
		</div>
		<?php
	} 
	?>
</div>