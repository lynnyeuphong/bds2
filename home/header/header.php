	<?php session_start(); ?>
	<link rel="stylesheet" href="js/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="styles.css">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<header class="header">
		<div class="header-top">
			<div class="logo">
				BDS Hà Thành
			</div>
			<div class="icon">
				<div class="link">
					<a href="">en</a>
					<a href="">vn</a>
				</div>
				<div class="font-awesome">
					<form action="timkiem.php">
						<input name="seach" type="search" placeholder="Search" >
					</form>
					
					<a href="form-admin.php" ><i class="fa fa-user" aria-hidden="true"></i></a>
				</div>

			</div>
			<div class="accout" style="
			position: absolute;
			top:0;
			right: 0;
			color:#fff;
			font-family: Roboto-Bold;
			font-size: 11px;
			text-transform: uppercase;">
			<?php 
			if (isset($_SESSION['username']) && $_SESSION['username']){

				echo 'xin chào '.$_SESSION['username']."<br/>";
				echo 'Click vào đây để <a href="logout.php">Logout</a>';
			}
			else{
				echo 'Bạn chưa đăng nhập';
			}
			?>
		</div>
		<a  href="cart.php" class="giohang" style=" position: absolute;
		bottom: 0;
		right: 0;
		color:#fff;">
		<i class="fa fa-shopping-cart" aria-hidden="true"></i>
	</a>
</div>
<nav class="nav-header">
	<ul>
		<li><a href="index.php">trang chủ</a></li>
		<li><a href="duan.php">dự án</a></li>
		<li><a href="tintuc.php?id=5">tin tức</a></li>
		<li><a href="banhang.php">siêu thị nhà đất</a></li>
		<li><a href="lienhe.php">liên hệ</a></li>
	</ul>
</nav>
</header>
<div class="hamburger-block">
	<div class="icon">
		<div class="link">
			<a href="">en</a>
			<a href="">vn</a>
		</div>
		<div class="font-awesome">
			<a href="#" class="seach"><i class="fa fa-search" aria-hidden="true"></i></a>
			<a href="#" ><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
		</div>
	</div>
	<div class="hamburger">
		<span class="bar bar1"></span>
		<span class="bar bar2"></span>
		<span class="bar bar3"></span>

	</div>
</div>
<script>
	jQuery(document).ready(function($) {
		$('.hamburger').click(function() {
			$('.header').toggleClass('menu-mobile-active');
			$('.header').removeClass('scrolled-header');
			$('.logo').removeClass('scrolled-logo');
			$('.hamburger').toggleClass('hamburger-active');
		});
	});
</script>

