	<link rel="stylesheet" href="js/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="header.css">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<header class="header">
		<div class="header-top">
			<div class="logo">
				cenhomes.vn
			</div>
			<div class="icon">
				<div class="link">
					<a href="">en</a>
					<a href="">vn</a>
				</div>
				<div class="font-awesome">
					<a href="#" class="seach"><i class="fa fa-search" aria-hidden="true"></i></a>
					<a href="form-admin.php" ><i class="fa fa-user" aria-hidden="true"></i></a>
				</div>

			</div>
		</div>
		<nav class="nav-header">
			<ul>
				<li><a href="http://localhost:8080/cenhomes/">trang chủ</a></li>
				<li><a href="#">giới thiệu</a></li>
				<li><a href="#">dự án</a></li>
				<li><a href="http://localhost:8080/cenhomes/tintuc/tintuc.php">tin tức</a></li>
				<li><a href="http://localhost:8080/cenhomes/sieuthinhadat/banhang.php">siêu thị nhà đất</a></li>
				<li><a href="http://localhost:8080/cenhomes/lienhe/lienhe.php">liên hệ</a></li>
			</ul>
		</nav>
	</header>
	<div class="hamburger-block">
		<div class="icon">
			<div class="link">
				<a href="">en</a>
				<a href="">vn</a>
			</div>
			<div class="font-awesome">
				<a href="#" class="seach"><i class="fa fa-search" aria-hidden="true"></i></a>
				<a href="#" ><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
			</div>
		</div>
		<div class="hamburger">
			<span class="bar bar1"></span>
			<span class="bar bar2"></span>
			<span class="bar bar3"></span>

		</div>
	</div>
	