
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Bất Động Sản Hà Thành</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="styles.css">
	<link rel="stylesheet" href="fonts.css">
	<link rel="stylesheet" href="js/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="swiper-4.4.6/dist/css/swiper.min.css">	
	<script src="js/bootstrap.min.js"></script>
	<script src="js/swiper.min.js"></script>
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/swiper.min.js"></script>	
	
</head>

<?php include_once ('home/header/header.php'); ?>
<main>
	<?php include_once ('home/baner/baner.php'); ?>
	<?php include_once ('home/intro/intro.php'); ?>
	<?php include_once ('section-video.php'); ?>
	<?php include_once ('home/list/list.php'); ?>
</main>
<?php include_once ('home/footer/footer.php'); ?>
<script>
	var swiper1 = new Swiper('#swiper-baner', {
		slidesPerView:"auto",
		autoplay: {
			delay: 5000,
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		pagination: {
			el: '#pagination-baner',
		},
	});
	var swiper2 = new Swiper('#swiper-intro', {
		slidesPerView:"auto",
		autoplay: {
			delay: 5000,
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		pagination: {
			el: '.swiper-pagination',
		},
	});
	var swiper3 = new Swiper('#swiper-news', {
		slidesPerView:"auto",
				// autoplay: {
				// 	delay: 5000,
				// },
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
				pagination: {
					el: '.swiper-pagination',
				},
			});
	var swiper4 = new Swiper('#swiper-prize', {
		slidesPerView:"3",
		breakpoints: {
			480: {
				slidesPerView: 1,
			},
			768: {
				slidesPerView: 2,
			},
			1024: {
				slidesPerView: 3,
			},
		},
		autoplay: {
			delay: 5000,
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		pagination: {
			el: '.swiper-pagination',
		},
	});
	var swiper5 = new Swiper('#panigation-prize',{
		pagination: {
			el: '#pagination-prize',
		},
	});
	var swiper6 = new Swiper('#swiper-contentmobile', {
		slidesPerView: 5,
		breakpoints: {
			320: {
				slidesPerView: 1,
			},
			480: {
				slidesPerView: 2,
			},
			768: {
				slidesPerView: 3,
			},
			1024: {
				slidesPerView: 4,
			},
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		pagination: {
			el: '.swiper-pagination',				
		},
	});
</script>
<script>
	jQuery(document).ready(function($) {
		$('.seach').click(function() {
			$('.header').toggleClass('seach-active');
		});
	});
</script>


