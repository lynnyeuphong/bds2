<link rel="stylesheet" type="text/css" href="ss2.css">
<div class="section section_2" data-section="section_2" >
	<div class="left" id="js-tab-ss2" data-aos="fade-right" data-aos-duration="2000">
		<div class="_title">
			Thiên thời
		</div>
		<div class="tab-menu-block">
			<a class="is-active" href="#js-tab-1" data-head="">tổng quan</a>
			<a class="" href="#js-tab-2" data-head="">Chủ đầu tư</a>
		</div>
		<div class="content">
			<div class="tab-left" id="js-tab-1" data-content="">
				<div class="the-text">
					<p class="_text">
						Khu đô thị sinh thái phức hợp đẳng cấp bậc nhất Thành phố Thái Bình. Lấy cảm hứng thiết kế từ hình ảnh cách điệu con sông uốn lượn bên những cánh đồng lúa bạt ngàn no đủ, Công viên chức năng ven sông Dragon riverside park là lá phổi xanh khổng lồ kiến tạo khu đô thị sinh thái Dragonhomes Eco city sở hữu tiện ích xứng tầm đỉnh cao cuộc sống thượng lưu giữa một miền thiên nhiên trong lành và căng tràn nhựa sống.
					</p>
					<p class="_text">
						Khu đô thị sinh thái phức hợp đẳng cấp bậc nhất Thành phố Thái Bình. Lấy cảm hứng thiết kế từ hình ảnh cách điệu con sông uốn lượn bên những cánh đồng lúa bạt ngàn no đủ
					</p>
				</div>
				<a class="_see_more" href="javascript:void(0);">Xem thêm</a>
			</div>
			<div class="tab-right" id="js-tab-2" data-content="">
				<div class="the-text">
					<p class="_text">
						ích xứng tầm đỉnh cao cuộc sống thượng lưu giữa một miền thiên nhiên trong lành và căng tràn nhựa sống.
					</p>
					<p class="_text">
						Khu đô thị sinh thái phức hợp đẳng cấp bậc nhất Thành phố  từ hình ảnh cách điệu con sông uốn lượn bên những cánh đồng lúa bạt ngàn no đủ
					</p>
				</div>
				<a class="_see_more" href="javascript:void(0);">Xem thêm</a>
			</div>
		</div>
	</div>
	<div class="right" style="background-image:url(imagaes/nguyen/VPTB_PC_ngay_PA2.jpg)">
		<div class="img" style="background-image:url(images/nguyen/VPTB_PC_ngay_PA2.jpg)">	
		</div>		
	</div>
</div>
<script>	
	(function($){
		$.fn.tabs = function(){
			var wrap = $(this);
			var head = wrap.find('[data-head]');
			var content = wrap.find('[data-content]');
			this.reset = (function(){ 
				head.not(head.first()).removeClass('is-active');
				content.not(content.first()).hide();
			}).call(this);
			this.headClick = head.click(function(event){
				event.preventDefault();
				if ( $(this).hasClass('is-active') ) {
					return false;
				}
				var content_target = $(this).attr('href');
				head.removeClass('is-active');
				content.hide();

				$(this).addClass('is-active');
				$(content_target).fadeIn();
			});
			return this;
		};
	});
	jQuery(document).ready(function($){
		$('#js-tab-ss2').tabs();
	});
</script>