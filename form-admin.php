<?php
//Khai báo sử dụng session
session_start();
if (isset($_SESSION['username']) && $_SESSION['username']){

	
	header('location:index.php');
	echo "<script type='text/javascript'>alert('ban da dang nhap rôi');</script>";
}
//Khai báo utf-8 để hiển thị được tiếng việt
header('Content-Type: text/html; charset=UTF-8');

//Xử lý đăng nhập
if (isset($_POST['dangnhap'])) 
{
    //Kết nối tới database
	include('config/config.php');
	$ketnoi = mysqli_connect("localhost", "root", "", "bds_hathanh");
    //Lấy dữ liệu nhập vào
	$username = addslashes($_POST['taikhoan']);
	$password = addslashes($_POST['password']);

    //Kiểm tra đã nhập đủ tên đăng nhập với mật khẩu chưa
	if (!$username || !$password) {
		echo "Vui lòng nhập đầy đủ tên đăng nhập và mật khẩu. <a href='javascript: history.go(-1)'>Trở lại</a>";
		exit;
	}

    // mã hóa pasword

    //Kiểm tra tên đăng nhập có tồn tại không
	$sql ="SELECT tai_khoan, mat_khau,id FROM tbl_nguoidung WHERE tai_khoan='$username'";
	$query=$ketnoi-> query($sql);
	$du_lieu = mysqli_query($ketnoi,$sql);//chay cau lenh sql
	$so_luong = mysqli_num_rows($du_lieu);// dem so dong
	if(
		$so_luong==0){

		echo "Tên đăng nhập này không tồn tại. Vui lòng kiểm tra lại. <a href='javascript: history.go(-1)'>Trở lại</a>";
		exit;
	}

    //Lấy mật khẩu trong database ra
	$row = mysqli_fetch_array($query);
    //So sánh 2 mật khẩu có trùng khớp hay không

	if ($password != $row['mat_khau']) {
		echo "<script>alert('sai mật khẩu!');</script>";
		exit;
	}

    //Lưu tên đăng nhập
	$_SESSION['username'] = $username;
	echo "<script>alert('xin chào $username');</script>" ;
	header('location:index.php');
}
?>
<link rel="stylesheet" href="./home/contact/contact.css">

<div class="section_contact"  style="background: black url(images/home/header-footer-s9/s9-bg.png)">
	<div class="form-title" data-aos="zoom-in" data-aos-duration="2000">
		<p>chào mừng bạn đến với BDS HÀ THÀNH</p>
		<p>Vui lòng đăng nhập để được sử dụng đấy đủ dịch vụ từ chúng tôi.<br>Chúng tôi sẽ liên hệ với quý khách trong thời gian sớm nhất. Xin cảm ơn! </p>
	</div>
	<form action="" method="POST"  id="js-form-register" class="form-content"  onsubmit="return checkForm()" name="form-contact">
		<input type="hidden" name="action" value="submit_contact">
		<input type="hidden" name="nonce" value="submit_contact">
		<div class="form all-form-fields">
			<input type="id"  name="taikhoan" id="taikhoan" class="input" placeholder="tài khoản">
			<input type="password"  name="password" id="password" class="input" placeholder="mật khẩu">
			<button type="submit" class="submit" name="dangnhap">đăng nhập<i class="fa fa-angle-right"></i></button>
			<button type="button"  class="submit" name="huy" onclick="Redirect()">đăng kí<i class="fa fa-angle-right"></i></button>
			<button type="button"  class="submit" name="trangchu" onclick="back()">trang chủ<i class="fa fa-angle-right"></i></button>
		</div>
		<div class="msg-form"></div>

	</form>
</div>
<script>
	function checkForm()
	{
		
		var taikhoan = document.getElementsByName("taikhoan")[0].value;
		var password = document.getElementsByName("password")[0].value;		
		if(taikhoan == '')
		{
			alert('Bạn phải nhập tài khoản');
			document.forms["form-contact"]["taikhoan"].focus();
			return false;
		}
		
		else if(password == '')
		{
			alert('Bạn phải nhập mạt khẩu');
			document.forms["form-contact"]["password"].focus();
			return false;
		}
		
		else return true;
	};
	function Redirect() {
		window.location="dangky.php";
	};
	function back() {
		window.location="index.php";
	};
</script>