<?php include ('home/header/header.php'); ?>
<?php $id=$_GET['id'];?>
<?php 

include('config/config.php');
$ketnoi = mysqli_connect("localhost", "root", "", "bds_hathanh");

?>
<link rel="stylesheet" href="tintuc.css">
<link rel="stylesheet" href="home/header/header.css">
<link rel="stylesheet" href="home/footer/footer.css">
<link rel="stylesheet" href="styles.css">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="fonts.css">
<link rel="stylesheet" href="js/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="swiper-4.4.6/dist/css/swiper.min.css"> 
<link rel="stylesheet" href="css/swiper.min.css"> 
<script src="js/bootstrap.min.js"></script>
<script src="js/swiper.min.js"></script>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/swiper.min.js"></script>
<div class="chitiettintuc">
  <div class="baner" style="background-image:url(images/banv/s4_img.png)">
  </div>
  <div class="colum">

    <div class="left">
      <?php 
      $sql="SELECT * FROM tbl_sanpham where id='$id'";
      $query = $ketnoi->query($sql);
      while ($rows = $query->fetch_assoc()){
        ?> 
        <h1>
          <?php echo $rows['tensp']; ?>
        </h1>
        <div class="text">
          <p>
            <?php echo $rows['masp']; ?>
          </p>
          <a href="#">chi tiết sản phẩm</a>
        </div>
        <div class="content">
          <div class="title">
            <?php echo $rows['gia']; ?>
          </div>
          <div class="img"style="background-image:url('admin/quanly/uploads/<?php echo $rows['anh1'] ?>')">
          </div>
          <p>

            <?php echo html_entity_decode( $rows['chitietsanpham']); ?>
          </p>
          <div class="evaluate">
            <div class="stars">
              <i class="fa fa-star-o active" aria-hidden="true"></i>
              <i class="fa fa-star-o" aria-hidden="true"></i>
              <i class="fa fa-star-o" aria-hidden="true"></i>
              <i class="fa fa-star-o" aria-hidden="true"></i>
              <i class="fa fa-star-o" aria-hidden="true"></i>
            </div>
            <div class="text">
              <p>5.0</p>
              <a href="#">3 đánh giá</a>
            </div>
          </div>
        </div>
        <div class="bottom">
          <div class="hastag">
            <a  href="#">#BDS</a>
            <a  href="#">#duan</a>
            <a  href="#">#canho</a>
          </div>
          <div class="social-chitiettintuc">
            <p> Chia sẻ :</p>
            <a href="#">
              <div class="img" style="background-image:url('images/chitiettintuc/facebook.png')"></div>
            </a>
            <a href="#">
              <div class="img" style="background-image:url('images/chitiettintuc/twitter.png')"></div>
            </a>
            <a href="#">
              <div class="img" style="background-image:url('images/chitiettintuc/linkedin.png')"></div>
            </a>
            <a href="#">
              <div class="img" style="background-image:url('images/chitiettintuc/youtube.png')"></div>
            </a>
          </div>
        </div>
        <?php
      } 
      ?>
    </div>
    <div class="right">
      <div class="news">
        <h3>sản phẩm liên quan</h3>
        <?php
        $sql="SELECT * FROM tbl_sanpham";  
        $query = $ketnoi->query($sql);
        while ($rows = $query->fetch_assoc()){
          ?>
          <a href="chitietsanpham.php?id=<?php echo $rows['id'] ?>" class="item">
            <div class="img"style="background-image:url('admin/quanly/uploads/<?php echo $rows['anh1'] ?>')">
            </div>
            <div class="text">
              <div class="title"><?php echo $rows['tensp'] ?>
            </div>  
            <p><?php echo $rows['gia'] ?></p>
          </div>
        </a>
        <?php
      } 
      ?>  
      <a  class='button' href="#">
        Xem thêm
      </a>  
    </div>
  </div>

</div>
</div>
<?php include_once ('home/footer/footer.php'); ?>
<script>
  jQuery(document).ready(function($) {
    $('.hamburger').click(function() {
      $('.header').toggleClass('menu-mobile-active');
      $('.header').removeClass('scrolled-header');
      $('.logo').removeClass('scrolled-logo');
      $('.hamburger').toggleClass('hamburger-active');
    });
  });
</script>